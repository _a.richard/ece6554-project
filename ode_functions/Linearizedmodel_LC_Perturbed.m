function xdot = f(t, x)
global A2 B2 K;

u=-K*x;
xdot=A2*x+B2*u;

end

