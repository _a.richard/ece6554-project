function xdot = f(t, x)
global A2 B2 K traj trajd;

tr = traj(t);
trd = trajd(t);

u = - K(1)*(x(1) - tr) - K(2)*(x(2) - trd) - K(3)*x(3) - K(4)*x(4);
xdot=A2*x+B2*u;

end

