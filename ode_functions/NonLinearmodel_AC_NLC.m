function xdot = f(t, x)
global g M L b Dx Dtheta Q B Am Bm A K;
global gamma_x gamma_r;
r = 0;

xm = x(1:4,1);
xc = x(5:8,1);
kx = x(9:12,1); 
kr = x(13);
P = lyap(Am',Q);
e = xc - xm;

xdot = zeros(13,1);
um = - K(1)*xm(1) - K(2)*xm(2) - K(3)*xm(3) - K(4)*xm(4);
xdot(1:4) = A*xm+B*um;
ui=kx'*xc+kr*r;
u = (b/(M*L-1))*((M*L - cos(x(7))*cos(x(7)))/b)*ui;
%u =  ui*;
xdot(5) = x(6);
% xdot(6) = (1/(M*L - cos(x(7))*cos(x(7))))*(L*sin(x(7))*x(8)*x(8) - g*cos(x(7))*sin(x(7)) - L*Dx*x(6) + Dtheta*cos(x(7))*x(8))  + L*u;%;(b*L)/(M*L - cos(x(7))*cos(x(7)))*u;
xdot(6) = (1/(M*L - cos(x(7))*cos(x(7))))*(L*sin(x(7))*x(8)*x(8) - g*cos(x(7))*sin(x(7)) - L*Dx*x(6) + Dtheta*cos(x(7))*x(8))  + (b*L)/(M*L - cos(x(7))*cos(x(7)))*u;
xdot(7) = x(8);
% xdot(8) = (1/(M*L - cos(x(7))*cos(x(7))))*(-cos(x(7))*sin(x(7))*x(8)*x(8) + M*g*sin(x(7)) + Dx*cos(x(7))*x(8) - M*Dtheta*x(8))  - b*cos(x(7))*u/(M*L-1);
xdot(8) = (1/(M*L - cos(x(7))*cos(x(7))))*(-cos(x(7))*sin(x(7))*x(8)*x(8) + M*g*sin(x(7)) + Dx*cos(x(7))*x(8) - M*Dtheta*x(8))  + (-b*cos(x(7)))/(M*L - cos(x(7))*cos(x(7)))*u;
xdot(9:12) = - xc * e' * P * B ; % kx
xdot(9:12) = - gamma_x.*xc * e' * P * B ; % kx
xdot(13) = - gamma_r .* r * e' * P * B;%kr
end


