function xdot = f(t, x)
global g2 M2 L2 b2 Dx2 Dtheta2 K traj trajd;

tr = traj(t);
trd = trajd(t);

u = - K(1)*(x(1) - tr) - K(2)*(x(2) - trd) - K(3)*x(3) - K(4)*x(4);
xdot = zeros(4,1);

xdot(1) = x(2);
xdot(2) = (1/(M2*L2 - cos(x(3))*cos(x(3))))*(L2*sin(x(3))*x(4)*x(4) - g2*cos(x(3))*sin(x(3)) - L2*Dx2*x(2) + Dtheta2*cos(x(3))*x(4))  + (b2*L2)/(M2*L2 - cos(x(3))*cos(x(3)))*u;
xdot(3) = x(4);
xdot(4) = (1/(M2*L2 - cos(x(3))*cos(x(3))))*(-cos(x(3))*sin(x(3))*x(4)*x(4) + M2*g2*sin(x(3)) + Dx2*cos(x(3))*x(4) - M2*Dtheta2*x(4))  + (-b2*cos(x(3)))/(M2*L2- cos(x(3))*cos(x(3)))*u;

end

