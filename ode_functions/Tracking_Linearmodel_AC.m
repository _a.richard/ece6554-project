function xdot = f(t, x)
global Q A B Am Bm gamma_x gamma_r K traj trajd

tr = traj(t);
trd = trajd(t);
r = tr*K(1) + trd*K(2);

xm = x(1:4,1);
xc = x(5:8,1);
kx = x(9:12,1); 
kr = x(13);
P = lyap(Am',Q);
e = xc - xm;

xdot = zeros(13,1);
% Write the reference system this because I couldn't get it to work under
% the usual form....
um = - K(1)*(xm(1) - tr) - K(2)*(xm(2) - trd) - K(3)*xm(3) - K(4)*xm(4);
xdot(1:4) = A*xm+B*um;
u = kx'*xc+kr*r;
xdot(5:8) = A*xc+B*u; % corrected system
xdot(9:12) = - gamma_x.*xc * e' * P * B; % kx
xdot(13) = - gamma_r * r * e' * P * B ; %kr

end
