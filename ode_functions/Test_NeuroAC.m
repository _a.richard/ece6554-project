function xdot = f(t, x)
global  Q B Am A K centers nb_neurons sigma;
global gamma;

traj = sin(t);

xm = x(1);
xc = x(2);
alpha = x(3:2+nb_neurons);
P = lyap(Am',Q);
e = xc - xm;
xh = e;

phi = NAC_PHI2(centers,sigma,xh);
fx = alpha'*phi';

xdot = zeros(13,1);
um = K*(xm - traj);
xdot(1) = A*xm+B*um;
u= K*(xc - traj) - fx;
xdot(2) = A*x(2) + 2 + 4*sin(x(2))*x(2)^2 + x(2)^2 + u;
xdot(3:2+nb_neurons) = gamma*e'*P*B*phi;
end


