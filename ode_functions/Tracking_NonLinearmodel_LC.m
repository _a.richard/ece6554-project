function xdot = f(t, x)
global g M L b Dx Dtheta K traj trajd;

tr = traj(t);
trd = trajd(t);

u = - K(1)*(x(1) - tr) - K(2)*(x(2) - trd) - K(3)*x(3) - K(4)*x(4);
xdot = zeros(4,1);

xdot(1) = x(2);
xdot(2) = (1/(M*L - cos(x(3))*cos(x(3))))*(L*sin(x(3))*x(4)*x(4) - g*cos(x(3))*sin(x(3)) - L*Dx*x(2) + Dtheta*cos(x(3))*x(4))  + (b*L)/(M*L - cos(x(3))*cos(x(3)))*u;
xdot(3) = x(4);
xdot(4) = (1/(M*L - cos(x(3))*cos(x(3))))*(-cos(x(3))*sin(x(3))*x(4)*x(4) + M*g*sin(x(3)) + Dx*cos(x(3))*x(4) - M*Dtheta*x(4))  + (-b*cos(x(3)))/(M*L - cos(x(3))*cos(x(3)))*u;

end

