function xdot = f(t, x)
global g M L b Dx Dtheta Q B Am Bm A K centers nb_neurons sigma alpha_dim;
global gamma;
r = 0;

xm = x(1:4,1);
xc = x(5:8,1);
alpha = x(9:8+alpha_dim);
P = lyap(Am',Q);
e = xc - xm;
xh = e(2:4);

phi = NAC_PHI(centers,sigma,xh);

fx = alpha'*phi';

xdot = zeros(13,1);
um = - K*xm;
xdot(1:4) = A*xm+B*um;
u = - K*xc - fx;
xdot(5) = x(6);
xdot(6) = (1/(M*L - cos(x(7))*cos(x(7))))*(L*sin(x(7))*x(8)*x(8) - g*cos(x(7))*sin(x(7)) - L*Dx*x(6) + Dtheta*cos(x(7))*x(8))  + (b*L)/(M*L - cos(x(7))*cos(x(7)))*u;
xdot(7) = x(8);
xdot(8) = (1/(M*L - cos(x(7))*cos(x(7))))*(-cos(x(7))*sin(x(7))*x(8)*x(8) + M*g*sin(x(7)) + Dx*cos(x(7))*x(8) - M*Dtheta*x(8))  + (-b*cos(x(7)))/(M*L - cos(x(7))*cos(x(7)))*u;
xdot(9:8+alpha_dim) = gamma*e'*P*B*phi;
end


