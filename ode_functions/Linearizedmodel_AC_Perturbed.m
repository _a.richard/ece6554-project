function xdot = f(t, x)
global Q A2 B2 Am Bm gamma_x gamma_r A B K
r = 0;

xm = x(1:4,1);
xc = x(5:8,1);
kx = x(9:12,1); 
kr = x(13);
P = lyap(Am',Q);
e = xc - xm;

xdot = zeros(13,1);
% Write the reference system this because I couldn't get it to work under
% the usual form....
um = - K(1)*xm(1) - K(2)*xm(2) - K(3)*xm(3) - K(4)*xm(4);
xdot(1:4) = A*xm+B*um;
u = kx'*xc+kr*r;
xdot(5:8) = A2*xc+B2*u; % corrected system
xdot(9:12) = - gamma_x.*xc * e' * P * B2 ; % kx
xdot(13) = - gamma_r * r * e' * P * B2 ; %kr

end
