function xdot = f(t, x)
global Q A2 B2 Am Bm gamma_x gamma_r A B K M2 L2 g2 Dx2 Dtheta2 b2 traj trajd;

tr = traj(t);
trd = trajd(t);
r = tr*K(1) + trd*K(2);

xm = x(1:4,1);
xc = x(5:8,1);
kx = x(9:12,1); 
kr = x(13);
P = lyap(Am',Q);
e = xc - xm;

xdot = zeros(13,1);
% Write the reference system this because I couldn't get it to work under
% the usual form....
um = - K(1)*(xm(1) - tr) - K(2)*(xm(2) - trd) - K(3)*xm(3) - K(4)*xm(4);
xdot(1:4) = A*xm+B*um;
u= kx'*xc + kr*r;
xdot(5) = x(6);
xdot(6) = (1/(M2*L2 - cos(x(7))*cos(x(7))))*(L2*sin(x(7))*x(8)*x(8) - g2*cos(x(7))*sin(x(7)) - L2*Dx2*x(6) + Dtheta2*cos(x(7))*x(8))  + (b2*L2)/(M2*L2 - cos(x(7))*cos(x(7)))*u;
xdot(7) = x(8);
xdot(8) = (1/(M2*L2 - cos(x(7))*cos(x(7))))*(-cos(x(7))*sin(x(7))*x(8)*x(8) + M2*g2*sin(x(7)) + Dx2*cos(x(7))*x(8) - M2*Dtheta2*x(8))  + (-b2*cos(x(7)))/(M2*L2 - cos(x(7))*cos(x(7)))*u;
xdot(9:12) = - gamma_x.*xc * e' * P * B2 ; % kx
xdot(13) = - gamma_r * r * e' * P * B2 ; %kr

end


