%% Linear Controller reaction to parameters changes (Tracking & Stabilization)
clc;
close all;
clear all;

%% Parameters Normal
global mc mp Jp l delta_theta delta_x g M L b Dx Dtheta R Q A B K Am Bm;

mc=0.5;
mp=0.1;
Jp=0.006;
l=0.3;
delta_theta=0.05;
delta_x=0.01;
g=9.8;

M=(mc+mp)/(mp*l);
L=Jp/(mp*l)+l;
b=1/(mp*l);
Dx=delta_x/(mp*l);
Dtheta=delta_theta/(mp*l);

%% Linear model
A=[0,1,0,0;
    0, -L*Dx/(M*L-1), -g/(M*L-1), Dtheta/(M*L-1);
    0, 0,0,1;
    0, Dx/(M*L-1), M*g/(M*L-1), -M*Dtheta/(M*L-1)];

B=[0;
    b*L/(M*L-1);
    0;
    -b/(M*L-1)];

% Use LQR to find the gains.
Q = eye(4);
R = 2;
[K,X,E]=lqr(A,B,Q,R);

Am = A - B*K;
Bm = [0;1;0;1];

%% Parameters Perturbed (+20%)
global mc2 mp2 Jp2 l2 delta_theta2 delta_x2 g2 M2 L2 b2 Dx2 Dtheta2;
global A2 B2;

mc2=mc*1.2;
mp2=mp*1.2;
Jp2=Jp*1.2;
l2=l*1.2;
delta_theta2=delta_theta*1.2;
delta_x2=delta_x*1.2;
g2=9.8;

M2=(mc2+mp2)/(mp2*l2);
L2=Jp2/(mp2*l2)+l2;
b2=1/(mp2*l2);
Dx2=delta_x2/(mp2*l2);
Dtheta2=delta_theta2/(mp2*l2);

%% Linear model perturbed
A2=[0,1,0,0;
    0, -L2*Dx2/(M2*L2-1), -g2/(M2*L2-1), Dtheta2/(M2*L2-1);
    0, 0,0,1;
    0, Dx2/(M2*L2-1), M2*g2/(M2*L2-1), -M2*Dtheta2/(M2*L2-1)];

B2=[0;
    b2*L2/(M2*L2-1);
    0;
    -b2/(M2*L2-1)];

%% Run Comparison Linear Controller on Linear System vs Perturbed-Linear System
tspan = [0 8];
  
x0 = [0.2;0;50*pi/180;0];
[t, x] = ode45( @Linearizedmodel_LC , tspan, x0);
[tnl, xnl] = ode45( @Linearizedmodel_LC_Perturbed , tspan, x0);
f1 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(t,x(:,1));
        plot(tnl,xnl(:,1));
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('position (m)')
    legend('linear','perturbed-linear')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(t,x(:,2));
        plot(tnl,xnl(:,2));
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    legend('linear','perturbed-linear')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(t,180*x(:,3)/pi);
        plot(tnl,180*xnl(:,3)/pi)
    hold off
    grid on;
    legend('linear','perturbed-linear')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(t,180*x(:,4)/pi);
        plot(tnl,180*xnl(:,4)/pi);
    hold off
    grid on;
    legend('linear','perturbed-linear')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('System stabilization with \theta_0 = 50� and x_0 = 0.2m');
saveas(f1,'Results/track_LQR_LS_vs_LSP.png');

ti = [1:5000];
ti = ti*8/5000;
u1_ref_stab = interp1(t,-K*x',ti);
u1_per_stab = interp1(tnl,-K*xnl',ti);
f2 = figure('Position',[0 0 800 400]);
    hold on
        plot(ti,u1_ref_stab)
        plot(ti,u1_per_stab)
    hold off
    grid on
    xlabel('time (s)')
    ylabel('command')
    legend('linear','perturbed-linear')
    title('Command applied to stabilize the system')
saveas(f2,'Results/track_LQR_LS_vs_LSP_command.png');


%% Run Comparison Linear Controller on Linear System vs Perturbed-Non-Linear System
tspan = [0 8];
  
x0 = [0.2;0;50*pi/180;0];
[t, x] = ode45( @Linearizedmodel_LC , tspan, x0);
[tnl, xnl] = ode45( @NonLinearmodel_LC , tspan, x0);
[tnl2, xnl2] = ode45( @NonLinearmodel_LC_Perturbed , tspan, x0);
f3 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(tnl,xnl(:,1));
        plot(tnl2,xnl2(:,1));
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('position (m)')
    legend('non-linear','non-linear-perturbed')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(tnl,xnl(:,2));
        plot(tnl2,xnl2(:,2));
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    legend('non-linear','non-linear-perturbed')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(tnl,180*xnl(:,3)/pi)
        plot(tnl2,180*xnl2(:,3)/pi);
    hold off
    grid on;
    legend('non-linear','non-linear-perturbed')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(tnl,180*xnl(:,4)/pi);
        plot(tnl2,180*xnl2(:,4)/pi);
    hold off
    grid on;
    legend('non-linear','non-linear-perturbed')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('Systems stabilization with \theta_0 = 50� and x_0 = 0.2m');
saveas(f3,'Results/track_LQR_NLS_vs_NLSP.png');

ti = [1:5000];
ti = ti*8/5000;
u2_ref = interp1(t,-K*x',ti);
u2_nl_stab = interp1(tnl,-K*xnl',ti);
u2_per_nl_stab = interp1(tnl2,-K*xnl2',ti);

f4 = figure('Position',[0 0 800 400]);
    hold on
        plot(ti,u2_nl_stab)
        plot(ti,u2_per_nl_stab)
    hold off
    grid on
    xlabel('time (s)')
    ylabel('command')
    legend('non-linear','non-linear-perturbed')
    title('Command applied to stabilize the system')
saveas(f4,'Results/track_LQR_NLS_vs_NLSP_command.png');

%% Tracking Linear Controller Linear Model Sin
global traj trajd;

traj = @tr1;
trajd = @tr1d;

tspan = [0 50];

x0 = [0;0;0;0];
[t, x] = ode45( @Tracking_Linearmodel_LC , tspan, x0);
[tnl, xnl] = ode45( @Tracking_NonLinearmodel_LC , tspan, x0);
[tnl2, xnl2] = ode45( @Tracking_NonLinearmodel_LC_Perturbed , tspan, x0);
xtrack = traj(tnl);

f5 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(tnl,xnl(:,1));
        plot(tnl2,xnl2(:,1));
        plot(tnl,xtrack,'--k');
    hold off
    grid on
    xlabel('time (s)')
    ylabel('position (m)')
    legend('non-linear','non-linear-perturbed','tracked')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(tnl,xnl(:,2));
        plot(tnl2,xnl2(:,2));
    hold off
    grid on
    legend('non-linear','non-linear-perturbed')
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(tnl,180*xnl(:,3)/pi);
        plot(tnl2,180*xnl2(:,3)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular position (�)')
    legend('non-linear','non-linear-perturbed')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(tnl,180*xnl(:,4)/pi);
        plot(tnl2,180*xnl2(:,4)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    legend('non-linear','non-linear-perturbed')
    title('angular velocity');
    sgtitle('Tracking of a sinus signal');
saveas(f5,'Results/track_LQR_NLS_vs_NLSP_sin.png');

ti = [1:5000];
ti = ti*50/5000;
u3_ref = interp1(t,-K*x',ti);
u3_nl_stab = interp1(tnl,-K*xnl',ti);
u3_per_nl_stab = interp1(tnl2,-K*xnl2',ti);


x3_track_stab = interp1(tnl,xtrack,ti);
x3_ref = interp1(t,x(:,1),ti);
x3_nl_stab = interp1(tnl,xnl(:,1),ti);
x3_per_nl_stab = interp1(tnl2,xnl2(:,1),ti);

f6 = figure('Position',[0 0 800 400]);
    hold on
        plot(ti,u3_nl_stab)
        plot(ti,u3_per_nl_stab)
    hold off
    grid on
    xlabel('time (s)')
    ylabel('command')
    legend('non-linear','non-linear-perturbed')
    title('Command applied to stabilize the system')
saveas(f6,'Results/track_LQR_NLS_vs_NLSP_sin_command.png');

%% Tracking Linear Controller Linear Model Square

traj = @tr2;
trajd = @tr2d;

tspan = [0 50];

x0 = [0;0;0;0];
[t, x] = ode45( @Tracking_Linearmodel_LC , tspan, x0);
[tnl, xnl] = ode45( @Tracking_NonLinearmodel_LC , tspan, x0);
[tnl2, xnl2] = ode45( @Tracking_NonLinearmodel_LC_Perturbed , tspan, x0);

xtrack = zeros(1,length(tnl));
for i=1:length(tnl)
    xtrack(i) = traj(tnl(i));
end

f7 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(tnl,xnl(:,1));
        plot(tnl2,xnl2(:,1));
        plot(tnl,xtrack,'--k');
    hold off
    grid on
    xlabel('time (s)')
    ylabel('position (m)')
    legend('non-linear','non-linear-perturbed','tracked')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(tnl,xnl(:,2));
        plot(tnl2,xnl2(:,2));
    hold off
    grid on
    legend('non-linear','non-linear-perturbed')
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(tnl,180*xnl(:,3)/pi);
        plot(tnl2,180*xnl2(:,3)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular position (�)')
    legend('non-linear','non-linear-perturbed')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(tnl,180*xnl(:,4)/pi);
        plot(tnl2,180*xnl2(:,4)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    legend('non-linear','non-linear-perturbed')
    title('angular velocity');
    sgtitle('Tracking of a square signal');
saveas(f7,'Results/track_LQR_NLS_vs_NLSP_square.png');

ti = [1:5000];
ti = ti*50/5000;
u4_ref = interp1(t,-K*x',ti);
u4_nl_stab = interp1(tnl,-K*xnl',ti);
u4_per_nl_stab = interp1(tnl2,-K*xnl2',ti);

x4_track_stab = interp1(tnl,xtrack,ti);
x4_ref = interp1(t,x(:,1),ti);
x4_nl_stab = interp1(tnl,xnl(:,1),ti);
x4_per_nl_stab = interp1(tnl2,xnl2(:,1),ti);

f8 = figure('Position',[0 0 800 400]);
    hold on
        plot(ti,u4_nl_stab)
        plot(ti,u4_per_nl_stab)
    hold off
    grid on
    xlabel('time (s)')
    ylabel('command')
    legend('non-linear','non-linear-perturbed')
    title('Command applied to stabilize the system')
saveas(f8,'Results/track_LQR_NLS_vs_NLSP_square_command.png');

%% Tracking Linear Controller Linear Model Saw-Tooth

traj = @tr3;
trajd = @tr3d;

tspan = [0 50];

x0 = [0;0;0;0];
[t, x] = ode45( @Tracking_Linearmodel_LC , tspan, x0);
[tnl, xnl] = ode45( @Tracking_NonLinearmodel_LC , tspan, x0);
[tnl2, xnl2] = ode45( @Tracking_NonLinearmodel_LC_Perturbed , tspan, x0);
xtrack = zeros(1,length(tnl));
for i=1:length(tnl)
    xtrack(i) = traj(tnl(i));
end

f7 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(tnl,xnl(:,1));
        plot(tnl2,xnl2(:,1));
        plot(tnl,xtrack,'--k');
    hold off
    grid on
    xlabel('time (s)')
    ylabel('position (m)')
    legend('non-linear','non-linear-perturbed','tracked')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(tnl,xnl(:,2));
        plot(tnl2,xnl2(:,2));
    hold off
    grid on
    legend('non-linear','non-linear-perturbed')
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(tnl,180*xnl(:,3)/pi);
        plot(tnl2,180*xnl2(:,3)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular position (�)')
    legend('non-linear','non-linear-perturbed')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(tnl,180*xnl(:,4)/pi);
        plot(tnl2,180*xnl2(:,4)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    legend('non-linear','non-linear-perturbed')
    title('angular velocity');
    sgtitle('Tracking of a reference a saw-tooth signal');
saveas(f7,'Results/track_LQR_NLS_vs_NLSP_saw_tooth.png');

ti = [1:5000];
ti = ti*50/5000;
u5_ref = interp1(t,-K*x',ti);
u5_nl_stab = interp1(tnl,-K*xnl',ti);
u5_per_nl_stab = interp1(tnl2,-K*xnl2',ti);

x5_track_stab = interp1(tnl,xtrack,ti);
x5_ref = interp1(t,x(:,1),ti);
x5_nl_stab = interp1(tnl,xnl(:,1),ti);
x5_per_nl_stab = interp1(tnl2,xnl2(:,1),ti);

f8 = figure('Position',[0 0 800 400]);
    hold on
        plot(ti,u5_nl_stab)
        plot(ti,u5_per_nl_stab)
    hold off
    grid on
    xlabel('time (s)')
    ylabel('command')
    legend('non-linear','non-linear-perturbed')
    title('Command applied to stabilize the system')
saveas(f8,'Results/track_LQR_NLS_vs_NLSP_saw_tooth_command.png');

%% Write to file
fileID = fopen('Results/LQR_track_stab.txt','w');
fprintf(fileID, '----------------------------------\n');
fprintf(fileID, '  Stabilization Linear Controler  \n');
fprintf(fileID, '----------------------------------\n');
fprintf(fileID, '           Linear System          \n');
fprintf(fileID, ' \n');
fprintf(fileID, 'ref vs perturbation (MSE): %f\n',mse(u1_ref_stab,u1_per_stab));
fprintf(fileID, ' \n');
fprintf(fileID, '----------------------------------\n');
fprintf(fileID, '          Non-Linear System       \n');
fprintf(fileID, ' \n');
fprintf(fileID, 'ref vs non-linear (MSE): %f\n',mse(u2_ref,u2_nl_stab));
fprintf(fileID, 'ref vs perturbed-non-linear (MSE): %f\n',mse(u2_ref,u2_per_nl_stab));
fprintf(fileID, ' \n');
fprintf(fileID, '----------------------------------\n');
fprintf(fileID, '            Tracking sin          \n');
fprintf(fileID, ' \n');
fprintf(fileID, 'Command:\n');
fprintf(fileID, 'ref vs non-linear (MSE): %f\n',mse(u3_ref,u3_nl_stab));
fprintf(fileID, 'ref vs non-linear-pertubed (MSE): %f\n',mse(u3_ref,u3_per_nl_stab));
fprintf(fileID, '\n');
fprintf(fileID, 'Error:\n');
fprintf(fileID, 'ref vs non-linear (MSE): %f\n',mse(x3_ref,x3_nl_stab ));
fprintf(fileID, 'ref vs non-linear-pertubed (MSE): %f\n',mse(x3_ref,x3_per_nl_stab));
fprintf(fileID, '\n');
fprintf(fileID, '----------------------------------\n');
fprintf(fileID, '          Tracking square         \n');
fprintf(fileID, ' \n');
fprintf(fileID, 'Command:\n');
fprintf(fileID, 'ref vs non-linear (MSE): %f\n',mse(u4_ref,u4_nl_stab));
fprintf(fileID, 'ref vs non-linear-pertubed (MSE): %f\n',mse(u4_ref,u4_per_nl_stab));
fprintf(fileID, '\n');
fprintf(fileID, 'Error:\n');
fprintf(fileID, 'ref vs non-linear (MSE): %f\n',mse(x4_ref,x4_nl_stab ));
fprintf(fileID, 'ref vs non-linear-pertubed (MSE): %f\n',mse(x4_ref,x4_per_nl_stab));
fprintf(fileID, '\n');
fprintf(fileID, '----------------------------------\n');
fprintf(fileID, '         Tracking saw-tooth       \n');
fprintf(fileID, ' \n');
fprintf(fileID, 'Command:\n');
fprintf(fileID, 'ref vs non-linear: %f\n',mse(u5_ref, u5_nl_stab));
fprintf(fileID, 'ref vs non-linear-perturbed: %f\n',mse(u5_ref, u5_per_nl_stab));
fprintf(fileID, '\n');
fprintf(fileID, 'Error:\n');
fprintf(fileID, 'ref vs non-linear (MSE): %f\n',mse(x5_ref,x5_nl_stab ));
fprintf(fileID, 'ref vs non-linear-pertubed (MSE): %f\n',mse(x5_ref,x5_per_nl_stab));
fprintf(fileID, '\n');
fprintf(fileID, '----------------------------------\n');
fclose(fileID);
type Results/LQR_track_stab.txt

%% Functions
% tracking of a sinus
% signal to track as tr, derivative of signal as trd
function y = tr1(x)
    y = 0.5*sin(x/4);
end
function y = tr1d(x)
    y = 0.5*1/4*cos(x/4);
end

% tracking of a step
% signal to track as tr, derivative of signal as trd
function y = tr2(x)
    if mod(x,16) > 8
        y = 0.5;
    else
        y = -0.5;
    end
end
function y = tr2d(x)
    y = 0;
end

% tracking of a saw-tooth
% signal to track as tr, derivative of signal as trd
function y = tr3(x)
    if mod(x,16) < 8
        y = mod(x,8)*0.25;
    else
        y = (8 - mod(x,8))*0.25;
    end
end
function y = tr3d(x)
    if mod(x,16) < 8
        y = 0.25;
    else
        y = -0.25;
    end
end