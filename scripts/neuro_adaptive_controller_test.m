%% Test of implementation for neuro-adaptive controller
global  Q B A K centers nb_neurons sigma Am;
global gamma;

nb_input = 1;
nb_neurons = 1001;
bound = 4;
sigma = bound/nb_neurons;
gamma = 10000;

centers  = meshgrid(-bound:2*bound/(nb_neurons-1):bound,1:1:nb_input);
alpha0 = rand(nb_neurons,1)*0;
k0 = -K;

A = 1;
Am = -1;
B = 1;
K = -2;
Q = 1;

tmax = 50;
tspan = [0 tmax];
xm0 = 4;
xc0 = xm0;
X0 = [xm0;xc0;alpha0];
[t, x] = ode45(@Test_NeuroAC , tspan, X0);
%% Figures
figure
hold on
plot(t,x(:,1))
plot(t,x(:,2))
hold off
legend('ref','neuro-adapt')

est_fx = [];
true_fx = [];
for i=1:length(t)
    est_fx(i) = x(i,3:end)*NAC_PHI2(centers,sigma,x(i,1)-x(i,2))';
    true_fx(i) = 4*sin(x(i,2))*x(i,2).^2 + 2 + x(i,2)^2;
end

figure
hold on
plot(t,true_fx)
plot(t,est_fx)
hold off
legend('true','estimate')