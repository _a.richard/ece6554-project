%% Linearized and Non-Linear Models with Linear Controller 
clc;
close all;
clear all;

%% Parameters
global mc mp Jp l delta_theta delta_x g M L b Dx Dtheta R Q A B K Am Bm;

mc=0.5;
mp=0.1;
Jp=0.006;
l=0.3;
delta_theta=0.05;
delta_x=0.01;
g=9.8;

M=(mc+mp)/(mp*l);
L=Jp/(mp*l)+l;
b=1/(mp*l);
Dx=delta_x/(mp*l);
Dtheta=delta_theta/(mp*l);

%% Linear model
A=[0,1,0,0;
    0, -L*Dx/(M*L-1), -g/(M*L-1), Dtheta/(M*L-1);
    0, 0,0,1;
    0, Dx/(M*L-1), M*g/(M*L-1), -M*Dtheta/(M*L-1)];

B=[0;
    b*L/(M*L-1);
    0;
    -b/(M*L-1)];

% Use LQR to find the gains.
Q = eye(4);
R = 2;
[K,X,E]=lqr(A,B,Q,R);

Am = A - B*K;
Bm = [0;1;0;1];

%% Run Linearized Model with Linear Controller:
tspan = [0 8];
t = {};
x = {};

% fixed x0 varitation of theta0
for i=1:7
    x0 = [0;0;i*10*pi/180;0];
    [t{i}, x{i}] = ode45( @Linearizedmodel_LC , tspan, x0);
end
f0 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,1));
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('position (m)')
    title('linear position');

    subplot(2,2,2);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,2));
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
    for i=1:7
        plot(t{i},180*x{i}(:,3)/pi);
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
    for i=1:7
        plot(t{i},180*x{i}(:,4)/pi);
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('Linear system stabilization with \theta_0 = 10� to 70� and x_0 = 0m');
saveas(f0,'Results/stab_LQR_LS_for_different_theta0.png');

% fixed theta0 varitation of x0
tspan = [0 8];
t = {};
x = {};
for i=1:7
    x0 = [i*0.3;0;30*pi/180;0];
    [t{i}, x{i}] = ode45( @Linearizedmodel_LC , tspan, x0);
end
f1 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,1));
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('position (m)')
    title('linear position');

    subplot(2,2,2);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,2));
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
    for i=1:7
        plot(t{i},180*x{i}(:,3)/pi);
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
    for i=1:7
        plot(t{i},180*x{i}(:,4)/pi);
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('Linear system stabilization with \theta_0 = 30� and x_0 = 0.3m to 2.1m');
saveas(f1,'Results/stab_LQR_LS_for_different_x0.png');
%% Run Non-Linear Model with Linear Controller:
tspan = [0 8];
t = {};
x = {};

% fixed x0 varitation of theta0
for i=1:7
    if i == 7
        tspan = [0 2];
    end
    x0 = [0;0;i*10*pi/180;0];
    [t{i}, x{i}] = ode45( @NonLinearmodel_LC , tspan, x0);
end
f2 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,1));
    end
    hold off
    grid on;
    ylim([-2 6.5])
    xlabel('time (s)')
    ylabel('position (m)')
    title('linear position');

    subplot(2,2,2);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,2));
    end
    hold off
    grid on;
    ylim([-10 10])
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
    for i=1:7
        plot(t{i},180*x{i}(:,3)/pi);
    end
    hold off
    grid on;
    ylim([-100 70])
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
    for i=1:7
        plot(t{i},180*x{i}(:,4)/pi);
    end
    hold off
    grid on;
    ylim([-300 100])
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('Non-Linear system stabilization with \theta_0 = 10� to 70� and x_0 = 0m');
saveas(f2,'Results/stab_LQR_NLS_for_different_theta0.png')

% fixed theta0 varitation of x0
tspan = [0 8];
t = {};
x = {};
for i=1:7
    x0 = [i*0.3;0;30*pi/180;0];
    [t{i}, x{i}] = ode45( @NonLinearmodel_LC , tspan, x0);
end
f3 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,1));
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('position (m)')
    title('linear position');

    subplot(2,2,2);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,2));
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
    for i=1:7
        plot(t{i},180*x{i}(:,3)/pi);
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
    for i=1:7
        plot(t{i},180*x{i}(:,4)/pi);
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('Non-Linear system stabilization with \theta_0 = 30� and x_0 = 0.3m to 2.1m');
saveas(f3,'Results/stab_LQR_NLS_for_different_x0.png');

%% Run Comparison Linear Controller on Linear-System vs Non-Linear System
tspan = [0 8];
  
x0 = [0.2;0;30*pi/180;0];
[t, x] = ode45( @Linearizedmodel_LC , tspan, x0);
[tnl, xnl] = ode45( @NonLinearmodel_LC , tspan, x0);
f4 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(t,x(:,1));
        plot(tnl,xnl(:,1));
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('position (m)')
    legend('linear','non-linear')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(t,x(:,2));
        plot(tnl,xnl(:,2));
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    legend('linear','non-linear')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(t,180*x(:,3)/pi);
        plot(tnl,180*xnl(:,3)/pi)
    hold off
    grid on;
    legend('linear','non-linear')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(t,180*x(:,4)/pi);
        plot(tnl,180*xnl(:,4)/pi);
    hold off
    grid on;
    legend('linear','non-linear')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('System stabilization with \theta_0 = 30� and x_0 = 0.2m');
saveas(f4,'Results/stab_LQR_LS_vs_NLS_30d_02m.png');
 
tspan = [0 8];  
x0 = [0.5;0;60*pi/180;0];
[t, x] = ode45( @Linearizedmodel_LC , tspan, x0);
[tnl, xnl] = ode45( @NonLinearmodel_LC , tspan, x0);
f5 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(t,x(:,1));
        plot(tnl,xnl(:,1));
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('position (m)')
    legend('linear','non-linear')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(t,x(:,2));
        plot(tnl,xnl(:,2));
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    legend('linear','non-linear')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(t,180*x(:,3)/pi);
        plot(tnl,180*xnl(:,3)/pi)
    hold off
    grid on;
    legend('linear','non-linear')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(t,180*x(:,4)/pi);
        plot(tnl,180*xnl(:,4)/pi);
    hold off
    grid on;
    legend('linear','non-linear')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('System stabilization with \theta_0 = 60� and x_0 = 0.5m'); 
saveas(f5,'Results/stab_LQR_LS_vs_NLS_60d_05m.png');