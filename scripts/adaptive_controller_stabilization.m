%% System Stabilization with Adaptive Controller
clc;
close all;
clear all;

%% Parameters Normal
global mc mp Jp l delta_theta delta_x g M L b Dx Dtheta R Q A B K Am Bm;

mc=0.5;
mp=0.1;
Jp=0.006;
l=0.3;
delta_theta=0.05;
delta_x=0.01;
g=9.8;

M=(mc+mp)/(mp*l);
L=Jp/(mp*l)+l;
b=1/(mp*l);
Dx=delta_x/(mp*l);
Dtheta=delta_theta/(mp*l);

%% Linear model
A=[0,1,0,0;
    0, -L*Dx/(M*L-1), -g/(M*L-1), Dtheta/(M*L-1);
    0, 0,0,1;
    0, Dx/(M*L-1), M*g/(M*L-1), -M*Dtheta/(M*L-1)];

B=[0;
    b*L/(M*L-1);
    0;
    -b/(M*L-1)];

% Use LQR to find the gains.
global P
Q = eye(4);
R = 2;
[K,X,E]=lqr(A,B,Q,R);
P = X;

Am = A - B*K;
Bm = [0;1;0;1];

%% Adaptive controller
global gamma_r gamma_x

gamma_x = [10;10;10;10];
gamma_r = 10;

%% Linear Model (Sanity check)
tspan = [0 8];
xc0 = [0;0;10*pi/180;0];
xm0 = xc0;
kx0 = -K';
kr0 = 1;
x0 = [xm0;xc0;kx0;kr0];

t = {};
x = {};

for i=1:7
    xc0 = [0;0;i*10*pi/180;0];
    xm0 = xc0;
    x0 = [xm0;xc0;kx0;kr0];
    [t{i}, x{i}] = ode45( @Linearizedmodel_AC , tspan, x0);
end
f0 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,1));
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('position (m)')
    title('linear position');

    subplot(2,2,2);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,2));
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
    for i=1:7
        plot(t{i},180*x{i}(:,3)/pi);
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
    for i=1:7
        plot(t{i},180*x{i}(:,4)/pi);
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('Linear system stabilization with \theta_0 = 10� to 70� and x_0 = 0m');
saveas(f0,'Results/stab_AC_LS_for_different_theta0.png');

f1= figure('Position',[0 0 800 400]);
    subplot(2,3,1);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,9));
    end
    hold off
    grid on;
    ylim([-30 30])
    xlabel('time (s)')
    ylabel('Kx_1')
    
    subplot(2,3,2);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,10));
    end
    hold off
    grid on;
    ylim([-20 30])
    xlabel('time (s)')
    ylabel('Kx_2')

    subplot(2,3,4);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,11));
    end
    hold off
    grid on;
    ylim([15 50])
    xlabel('time (s)')
    ylabel('Kx_3')
    
    subplot(2,3,5);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,12));
    end
    hold off
    grid on;
    ylim([0 50])
    xlabel('time (s)')
    ylabel('Kx_4')
    subplot(2,3,[3 6]);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,13));
    end
    hold off
    grid on;
    ylim([0.5 1.5])
    xlabel('time (s)')
    ylabel('Kr')
    sgtitle('AC gains LS stabilization with \theta_0 = 10� to 70� and x_0 = 0m');
saveas(f1,'Results/stab_AC_LS_gains_for_different_theta0.png');

%% Non-Linear Model
tspan = [0 8];
xc0 = [0;0;10*pi/180;0];
xm0 = xc0;
kx0 = -K';
kr0 = 1;
x0 = [xm0;xc0;kx0;kr0];

t = {};
x = {};

for i=1:7
    if i==7
       tspan = [0 2]; 
    end
    xc0 = [0;0;i*10*pi/180;0];
    xm0 = xc0;
    x0 = [xm0;xc0;kx0;kr0];
    [t{i}, x{i}] = ode45(@NonLinearmodel_AC , tspan, x0);
end
f2 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,5));
    end
    hold off
    grid on;
    ylim([-2 6.5])
    xlabel('time (s)')
    ylabel('position (m)')
    title('linear position');

    subplot(2,2,2);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,6));
    end
    hold off
    grid on;
    ylim([-10 10])
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
    for i=1:7
        plot(t{i},180*x{i}(:,7)/pi);
    end
    hold off
    grid on;
    ylim([-100 70])
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');
    
    subplot(2,2,4);
    hold on
    for i=1:7
        plot(t{i},180*x{i}(:,8)/pi);
    end
    hold off
    grid on;
    ylim([-300 100])
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('Linear system stabilization with \theta_0 = 10� to 70� and x_0 = 0m');
saveas(f2,'Results/stab_AC_NLS_for_different_theta0.png');

f3= figure('Position',[0 0 800 400]);
    subplot(2,3,1);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,9));
    end
    hold off
    grid on;
    ylim([-30 30])
    xlabel('time (s)')
    ylabel('Kx_1')
    
    subplot(2,3,2);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,10));
    end
    hold off
    grid on;
    ylim([-20 30])
    xlabel('time (s)')
    ylabel('Kx_2')

    subplot(2,3,4);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,11));
    end
    hold off
    grid on;
    ylim([15 50])
    xlabel('time (s)')
    ylabel('Kx_3')
    
    subplot(2,3,5);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,12));
    end
    hold off
    grid on;
    ylim([0 50])
    xlabel('time (s)')
    ylabel('Kx_4')
    subplot(2,3,[3 6]);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,13));
    end
    hold off
    grid on;
    ylim([0.5 1.5])
    xlabel('time (s)')
    ylabel('Kr')
    sgtitle('AC gains NL stabilization with \theta_0 = 10� to 70� and x_0 = 0m');
saveas(f3,'Results/stab_AC_NLS_gains_for_different_theta0.png');

%% Non-Linear Model with Non-Linearity Compensation
tspan = [0 8];
xc0 = [0;0;10*pi/180;0];
xm0 = xc0;
kx0 = -K';
kr0 = 1;
x0 = [xm0;xc0;kx0;kr0];

t = {};
x = {};

for i=1:7
    if i==7
       tspan = [0 2]; 
    end
    xc0 = [0;0;i*10*pi/180;0];
    xm0 = xc0;
    x0 = [xm0;xc0;kx0;kr0];
    [t{i}, x{i}] = ode45(@NonLinearmodel_AC_NLC , tspan, x0);
end
f4= figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,5));
    end
    hold off
    grid on;
    ylim([-2 6.5])
    xlabel('time (s)')
    ylabel('position (m)')
    title('linear position');

    subplot(2,2,2);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,6));
    end
    hold off
    grid on;
    ylim([-10 10])
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
    for i=1:7
        plot(t{i},180*x{i}(:,7)/pi);
    end
    hold off
    grid on;
    ylim([-100 70])
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');
    
    subplot(2,2,4);
    hold on
    for i=1:7
        plot(t{i},180*x{i}(:,8)/pi);
    end
    hold off
    grid on;
    ylim([-300 100])
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('AC with NLC on NLS stabilization with \theta_0 = 10� to 70� and x_0 = 0m');
saveas(f4,'Results/stab_AC_NLC_NLS_for_different_theta0.png');

f5= figure('Position',[0 0 800 400]);
    subplot(2,3,1);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,9));
    end
    hold off
    grid on;
    ylim([-30 30])
    xlabel('time (s)')
    ylabel('Kx_1')
    
    subplot(2,3,2);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,10));
    end
    hold off
    grid on;
    ylim([-20 30])
    xlabel('time (s)')
    ylabel('Kx_2')

    subplot(2,3,4);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,11));
    end
    hold off
    grid on;
    ylim([15 50])
    xlabel('time (s)')
    ylabel('Kx_3')
    
    subplot(2,3,5);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,12));
    end
    hold off
    grid on;
    ylim([0 50])
    xlabel('time (s)')
    ylabel('Kx_4')
    subplot(2,3,[3 6]);
    hold on
    for i=1:7
        plot(t{i},x{i}(:,13));
    end
    hold off
    grid on;
    ylim([0.5 1.5])
    xlabel('time (s)')
    ylabel('Kr')
    sgtitle('AC gains NLS stabilization with \theta_0 = 10� to 70� and x_0 = 0m');
saveas(f5,'Results/stab_AC_NLC_NLS_gains_for_different_theta0.png');