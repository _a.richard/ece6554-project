%% Linearized and Non-Linear Models Models with Linear Tracking Controller
clc;
close all;
clear all;

%% Parameters
global mc mp Jp l delta_theta delta_x g M L b Dx Dtheta R Q A B K Am Bm;

mc=0.5;
mp=0.1;
Jp=0.006;
l=0.3;
delta_theta=0.05;
delta_x=0.01;
g=9.8;

M=(mc+mp)/(mp*l);
L=Jp/(mp*l)+l;
b=1/(mp*l);
Dx=delta_x/(mp*l);
Dtheta=delta_theta/(mp*l);

%% Linear model
A=[0,1,0,0;
    0, -L*Dx/(M*L-1), -g/(M*L-1), Dtheta/(M*L-1);
    0, 0,0,1;
    0, Dx/(M*L-1), M*g/(M*L-1), -M*Dtheta/(M*L-1)];

B=[0;
    b*L/(M*L-1);
    0;
    -b/(M*L-1)];

% Use LQR to find the gains.
Q = eye(4);
R = 2;
[K,X,E]=lqr(A,B,Q,R);

Am = A - B*K;
Bm = [0;1;0;1];

%% Tracking Linear Model
global traj trajd;

%% tracking of a sinus
traj = @tr1;
trajd = @tr1d;

tspan = [0 50];

x0 = [0;0;0;0];
[t, x] = ode45( @Tracking_Linearmodel_LC , tspan, x0);
[tnl, xnl] = ode45( @Tracking_NonLinearmodel_LC , tspan, x0);
xtrack = traj(t);

f0 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(t,x(:,1));
        plot(tnl,xnl(:,1));
        plot(t,xtrack,'--k');
    hold off
    grid on
    xlabel('time (s)')
    ylabel('position (m)')
    legend('linear','non-linear','tracked')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(t,x(:,2));
        plot(tnl,xnl(:,2));
    hold off
    grid on
    xlabel('time (s)')
    legend('linear','non-linear')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(t,180*x(:,3)/pi);
        plot(tnl,180*xnl(:,3)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular position (�)')
    legend('linear','non-linear')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(t,180*x(:,4)/pi);
        plot(tnl,180*xnl(:,4)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    legend('linear','non-linear')
    title('angular velocity');
    sgtitle('Tracking of a reference signal 1/2*sin(1/4t)');
saveas(f0,'Results/track_LQR_LS_vs_NLS_sin.png');

%% tracking of a step
traj = @tr2;
trajd = @tr2d;

tspan = [0 50];

x0 = [0;0;0;0];
[t, x] = ode45( @Tracking_Linearmodel_LC , tspan, x0);
[tnl, xnl] = ode45( @Tracking_NonLinearmodel_LC , tspan, x0);

xtrack = zeros(1,length(t));
for i=1:length(t)
    xtrack(i) = traj(t(i));
end

f1 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(t,x(:,1));
        plot(tnl,xnl(:,1));
        plot(t,xtrack,'--k');
    hold off
    grid on
    xlabel('time (s)')
    ylabel('position (m)')
    legend('linear','non-linear','tracked')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(t,x(:,2));
        plot(tnl,xnl(:,2));
    hold off
    grid on
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    legend('linear','non-linear')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(t,180*x(:,3)/pi);
        plot(tnl,180*xnl(:,3)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular position (�)')
    legend('linear','non-linear')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(t,180*x(:,4)/pi);
        plot(tnl,180*xnl(:,4)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    legend('linear','non-linear')
    title('angular velocity');
    sgtitle('Tracking of a reference signal step');
saveas(f1,'Results/track_LQR_LS_vs_NLS_step.png');

%% tracking of a saw-tooth
traj = @tr3;
trajd = @tr3d;

tspan = [0 50];

x0 = [0;0;0;0];
[t, x] = ode45( @Tracking_Linearmodel_LC , tspan, x0);
[tnl, xnl] = ode45( @Tracking_NonLinearmodel_LC , tspan, x0);

xtrack = zeros(1,length(t));
for i=1:length(t)
    xtrack(i) = traj(t(i));
end

f2 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(t,x(:,1));
        plot(tnl,xnl(:,1));
        plot(t,xtrack,'--k');
    hold off
    grid on
    xlabel('time (s)')
    ylabel('position (m)')
    legend('linear','non-linear','tracked')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(t,x(:,2));
        plot(tnl,xnl(:,2));
    hold off
    grid on
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    legend('linear','non-linear')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(t,180*x(:,3)/pi);
        plot(tnl,180*xnl(:,3)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular position (�)')
    legend('linear','non-linear')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(t,180*x(:,4)/pi);
        plot(tnl,180*xnl(:,4)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    legend('linear','non-linear')
    title('angular velocity');
    sgtitle('Tracking of a reference signal saw-tooth');
saveas(f2,'Results/track_LQR_LS_vs_NLS_saw-tooth.png');