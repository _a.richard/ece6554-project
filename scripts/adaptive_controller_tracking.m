%% System Tracking with Adaptive Controller
clc;
close all;
clear all;

%% Parameters Normal
global mc mp Jp l delta_theta delta_x g M L b Dx Dtheta R Q A B K Am Bm;

mc=0.5;
mp=0.1;
Jp=0.006;
l=0.3;
delta_theta=0.05;
delta_x=0.01;
g=9.8;

M=(mc+mp)/(mp*l);
L=Jp/(mp*l)+l;
b=1/(mp*l);
Dx=delta_x/(mp*l);
Dtheta=delta_theta/(mp*l);

%% Linear model
A=[0,1,0,0;
    0, -L*Dx/(M*L-1), -g/(M*L-1), Dtheta/(M*L-1);
    0, 0,0,1;
    0, Dx/(M*L-1), M*g/(M*L-1), -M*Dtheta/(M*L-1)];

B=[0;
    b*L/(M*L-1);
    0;
    -b/(M*L-1)];

% Use LQR to find the gains.
global P
Q = eye(4);
R = 2;
[K,X,E]=lqr(A,B,Q,R);
P = X;

Am = A - B*K;
Bm = [0;1;0;1];

%% Parameters Perturbed (+20%)
global mc2 mp2 Jp2 l2 delta_theta2 delta_x2 g2 M2 L2 b2 Dx2 Dtheta2;
global A2 B2;

mc2=mc*1.2;
mp2=mp*1.2;
Jp2=Jp*1.2;
l2=l*1.2;
delta_theta2=delta_theta*1.2;
delta_x2=delta_x*1.2;
g2=9.8;

M2=(mc2+mp2)/(mp2*l2);
L2=Jp2/(mp2*l2)+l2;
b2=1/(mp2*l2);
Dx2=delta_x2/(mp2*l2);
Dtheta2=delta_theta2/(mp2*l2);

%% Linear model perturbed
A2=[0,1,0,0;
    0, -L2*Dx2/(M2*L2-1), -g2/(M2*L2-1), Dtheta2/(M2*L2-1);
    0, 0,0,1;
    0, Dx2/(M2*L2-1), M2*g2/(M2*L2-1), -M2*Dtheta2/(M2*L2-1)];

B2=[0;
    b2*L2/(M2*L2-1);
    0;
    -b2/(M2*L2-1)];

%% Tracking Adaptive Controller Linearized System
global gamma_r gamma_x traj trajd
tmax = 50;

traj = @tr1;
trajd = @tr1d;

tspan = [0 tmax];
gamma_x = [10;10;10;10];
gamma_r = 10;

xc0 = [0;0;0*pi/180;0];
xm0 = xc0;
kx0 = -K';
kr0 = 1;
x0 = [xm0;xc0;kx0;kr0];
[t, x] = ode45( @Tracking_Linearmodel_AC , tspan, x0);
xtrack = zeros(1,length(t));
for i=1:length(t)
    xtrack(i) = traj(t(i));
end
f1 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(t,x(:,1));
        plot(t,x(:,5));
        plot(t,xtrack,'--k');
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('position (m)')
    legend('ref','adaptive (LS)','tracked')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(t,x(:,2));
        plot(t,x(:,6));
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    legend('ref','adaptive (LS)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(t,180*x(:,3)/pi);
        plot(t,180*x(:,7)/pi);
    hold off
    grid on;
    legend('ref','adaptive (LS)')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(t,180*x(:,4)/pi);
        plot(t,180*x(:,8)/pi);
    hold off
    grid on;
    legend('ref','adaptive (LS)')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('System tracking of a sinus signal');
saveas(f1,'Results/track_AC_LS_sin.png');

f2 = figure('Position',[0 0 800 400]);
    subplot(2,3,1);
    plot(t,x(:,9))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_1')
    title('Kx_1');
    xlim([0 tmax]);
    subplot(2,3,2);
    plot(t,x(:,10))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_2')
    title('Kx_2');
    xlim([0 tmax]);
    subplot(2,3,4);
    plot(t,x(:,11))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_3')
    title('Kx_3');
    xlim([0 tmax]);
    subplot(2,3,5);
    plot(t,x(:,12))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_4')
    title('Kx_4');
    xlim([0 tmax]);
    subplot(2,3,[3 6]);
    plot(t,x(:,13))
    grid on;
    xlabel('time (s)')
    ylabel('Kr')
    title('Kr');  
    xlim([0 tmax]);
    sgtitle('system gains when tracking a sinus');
saveas(f2,'Results/track_AC_LS_gains_sin.png');

%% Adaptive Controller Perturbed Linearized System
tspan = [0 tmax];
[t, x] = ode45( @Tracking_Linearmodel_AC_Perturbed , tspan, x0);
xtrack = zeros(1,length(t));
for i=1:length(t)
    xtrack(i) = traj(t(i));
end
f3 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(t,x(:,1));
        plot(t,x(:,5));
        plot(t,xtrack,'--k');
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('position (m)')
    legend('ref','adaptive (LSP)','tracked')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(t,x(:,2));
        plot(t,x(:,6));
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    legend('ref','adaptive (LSP)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(t,180*x(:,3)/pi);
        plot(t,180*x(:,7)/pi);
    hold off
    grid on;
    legend('ref','adaptive (LSP)')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(t,180*x(:,4)/pi);
        plot(t,180*x(:,8)/pi);
    hold off
    grid on;
    legend('ref','adaptive (LSP)')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('System tracking of a sinus signal');
saveas(f3,'Results/track_AC_LSP_sin.png');

f4 = figure('Position',[0 0 800 400]);
    subplot(2,3,1);
    plot(t,x(:,9))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_1')
    title('Kx_1');
    xlim([0 tmax]);
    subplot(2,3,2);
    plot(t,x(:,10))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_2')
    title('Kx_2');
    xlim([0 tmax]);
    subplot(2,3,4);
    plot(t,x(:,11))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_3')
    title('Kx_3');
    xlim([0 tmax]);
    subplot(2,3,5);
    plot(t,x(:,12))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_4')
    title('Kx_4');
    xlim([0 tmax]);
    subplot(2,3,[3 6]);
    plot(t,x(:,13))
    grid on;
    xlabel('time (s)')
    ylabel('Kr')
    title('Kr');  
    xlim([0 tmax]);
    sgtitle('system gains when tracking a sinus');
saveas(f4,'Results/track_AC_LSP_gains_sin.png');


%% Adaptive Controller Non-Linear System
tspan = [0 tmax];
[t, x] = ode45( @Tracking_NonLinearmodel_AC_Perturbed , tspan, x0);
xtrack = zeros(1,length(t));
for i=1:length(t)
    xtrack(i) = traj(t(i));
end
f5 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(t,x(:,1));
        plot(t,x(:,5));
        plot(t,xtrack,'--k');
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('position (m)')
    legend('ref','adaptive (NLS)','tracked')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(t,x(:,2));
        plot(t,x(:,6));
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    legend('ref','adaptive (NLS)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(t,180*x(:,3)/pi);
        plot(t,180*x(:,7)/pi);
    hold off
    grid on;
    legend('ref','adaptive (NLS)')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(t,180*x(:,4)/pi);
        plot(t,180*x(:,8)/pi);
    hold off
    grid on;
    legend('ref','adaptive (NLS)')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('System tracking of a sinus signal');
saveas(f5,'Results/track_AC_NLS_sin.png');

f6 = figure('Position',[0 0 800 400]);
    subplot(2,3,1);
    plot(t,x(:,9))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_1')
    title('Kx_1');
    xlim([0 tmax]);
    subplot(2,3,2);
    plot(t,x(:,10))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_2')
    title('Kx_2');
    xlim([0 tmax]);
    subplot(2,3,4);
    plot(t,x(:,11))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_3')
    title('Kx_3');
    xlim([0 tmax]);
    subplot(2,3,5);
    plot(t,x(:,12))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_4')
    title('Kx_4');
    xlim([0 tmax]);
    subplot(2,3,[3 6]);
    plot(t,x(:,13))
    grid on;
    xlabel('time (s)')
    ylabel('Kr')
    title('Kr');  
    xlim([0 tmax]);
    sgtitle('system gains when tracking a sinus');
saveas(f6,'Results/track_AC_NLS_gains_sin.png');

%% Adaptive Controller Perturbed Non-Linear System Sinus
traj = @tr1;
trajd = @tr1d;
tspan = [0 tmax];
xm0 = xc0;
kx0 = -K';
kr0 = 1;
x0 = [xm0;xc0;kx0;kr0];
[t, x] = ode45( @Tracking_NonLinearmodel_AC_Perturbed , tspan, x0);
kx0 = mean(x(end-50:end,9:12))';
kr0 = mean(x(end-50:end,13));
x0 = [xm0;xc0;kx0;kr0];
[t2, x2] = ode45( @Tracking_NonLinearmodel_AC_Perturbed , tspan, x0);
[t3, x3] = ode45( @Tracking_NonLinearmodel_LC_Perturbed , tspan, xc0);
xtrack = zeros(1,length(t));
xtrackd = zeros(1,length(t));
for i=1:length(t)
    xtrack(i) = traj(t(i));
    xtrackd(i) = trajd(t(i));
end
f7 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(t,x(:,1));
        plot(t,x(:,5));
        plot(t,xtrack,'--k');
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('position (m)')
    legend('ref','adaptive (NLSP)','tracked')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(t,x(:,2));
        plot(t,x(:,6));
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    legend('ref','adaptive (NLSP)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(t,180*x(:,3)/pi);
        plot(t,180*x(:,7)/pi);
    hold off
    grid on;
    legend('ref','adaptive (NLSP)')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(t,180*x(:,4)/pi);
        plot(t,180*x(:,8)/pi);
    hold off
    grid on;
    legend('ref','adaptive (NLSP)')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('System tracking of a sinus signal');
saveas(f7,'Results/track_AC_NLSP_sin.png');

f8 = figure('Position',[0 0 800 400]);
    subplot(2,3,1);
    plot(t,x(:,9))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_1')
    title('Kx_1');
    xlim([0 tmax]);
    subplot(2,3,2);
    plot(t,x(:,10))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_2')
    title('Kx_2');
    xlim([0 tmax]);
    subplot(2,3,4);
    plot(t,x(:,11))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_3')
    title('Kx_3');
    xlim([0 tmax]);
    subplot(2,3,5);
    plot(t,x(:,12))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_4')
    title('Kx_4');
    xlim([0 tmax]);
    subplot(2,3,[3 6]);
    plot(t,x(:,13))
    grid on;
    xlabel('time (s)')
    ylabel('Kr')
    title('Kr');  
    xlim([0 tmax]);
    sgtitle('system gains when tracking a sinus');
saveas(f8,'Results/track_AC_NLSP_gains_sin.png');


% that's ugly refactor it if time allows. Does u computation + interpolation 
ti = [1:50000];
ti = ti*50/50000;
xtrack2 = zeros(1,length(t2));
xtrackd2 = zeros(1,length(t2));
for i=1:length(t2)
    xtrack2(i) = traj(t2(i));
    xtrackd2(i) = trajd(t2(i));
end
r = xtrack*K(1) + xtrackd*K(2);
r2 = xtrack2*K(1) + xtrackd2*K(2);

tmp = [];
tmp2 = [];
for i=1:length(t)
    tmp(i) = x(i,9:12)*x(i,5:8)'+x(i,13).*r(i);
end
for i=1:length(t2)
    tmp2(i) = x2(i,9:12)*x2(i,5:8)'+x2(i,13).*r(i);
end
u1_ref = interp1(t,-K*x(:,1:4)',ti);
u1_ac = interp1(t,tmp,ti);
u1_ac2 = interp1(t2,tmp2,ti);
u1_lc = interp1(t3,-K*x3(:,1:4)',ti);

x1_track_stab = interp1(t,xtrack,ti);
x1_ref = interp1(t,x(:,1),ti);
x1_nlpac = interp1(t,x(:,5),ti);
x1_nlpac2 = interp1(t2,x2(:,5),ti);
x1_nlplc = interp1(t3,x3,ti);

%% Adaptive Controller Perturbed Non-Linear System Square
traj = @tr2;
trajd = @tr2d;
tmax = 300;
tspan = [0 tmax];
xm0 = xc0;
kx0 = -K';
kr0 = 1;
x0 = [xm0;xc0;kx0;kr0];
[t, x] = ode45( @Tracking_NonLinearmodel_AC_Perturbed , tspan, x0);
kx0 = mean(x(end-50:end,9:12))';
kr0 = mean(x(end-50:end,13));
x0 = [xm0;xc0;kx0;kr0];
[t2, x2] = ode45( @Tracking_NonLinearmodel_AC_Perturbed , tspan, x0);
[t3, x3] = ode45( @Tracking_NonLinearmodel_LC_Perturbed , tspan, xc0);
xtrack = zeros(1,length(t));
xtrackd = zeros(1,length(t));
for i=1:length(t)
    xtrack(i) = traj(t(i));
    xtrackd(i) = trajd(t(i));
end
f9 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(t,x(:,1));
        plot(t,x(:,5));
        plot(t,xtrack,'--k');
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('position (m)')
    legend('ref','adaptive (NLSP)','tracked')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(t,x(:,2));
        plot(t,x(:,6));
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    legend('ref','adaptive (NLSP)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(t,180*x(:,3)/pi);
        plot(t,180*x(:,7)/pi);
    hold off
    grid on;
    legend('ref','adaptive (NLSP)')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(t,180*x(:,4)/pi);
        plot(t,180*x(:,8)/pi);
    hold off
    grid on;
    legend('ref','adaptive (NLSP)')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('System tracking of a sinus signal');
saveas(f9,'Results/track_AC_NLSP_square.png');

f10 = figure('Position',[0 0 800 400]);
    subplot(2,3,1);
    plot(t,x(:,9))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_1')
    title('Kx_1');
    xlim([0 tmax]);
    subplot(2,3,2);
    plot(t,x(:,10))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_2')
    title('Kx_2');
    xlim([0 tmax]);
    subplot(2,3,4);
    plot(t,x(:,11))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_3')
    title('Kx_3');
    xlim([0 tmax]);
    subplot(2,3,5);
    plot(t,x(:,12))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_4')
    title('Kx_4');
    xlim([0 tmax]);
    subplot(2,3,[3 6]);
    plot(t,x(:,13))
    grid on;
    xlabel('time (s)')
    ylabel('Kr')
    title('Kr');  
    xlim([0 tmax]);
    sgtitle('system gains when tracking a sinus');
saveas(f10,'Results/track_AC_NLSP_gains_square.png');

% that's ugly refactor it if time allows. Does u computation + interpolation 
ti = [1:50000];
ti = ti*50/50000;
xtrack2 = zeros(1,length(t2));
xtrackd2 = zeros(1,length(t2));
for i=1:length(t2)
    xtrack2(i) = traj(t2(i));
    xtrackd2(i) = trajd(t2(i));
end
r = xtrack*K(1) + xtrackd*K(2);
r2 = xtrack2*K(1) + xtrackd2*K(2);

tmp = [];
tmp2 = [];
for i=1:length(t)
    tmp(i) = x(i,9:12)*x(i,5:8)'+x(i,13).*r(i);
end
for i=1:length(t2)
    tmp2(i) = x2(i,9:12)*x2(i,5:8)'+x2(i,13).*r(i);
end
u2_ref = interp1(t,-K*x(:,1:4)',ti);
u2_ac = interp1(t,tmp,ti);
u2_ac2 = interp1(t2,tmp2,ti);
u2_lc = interp1(t3,-K*x3(:,1:4)',ti);

x2_track_stab = interp1(t,xtrack,ti);
x2_ref = interp1(t,x(:,1),ti);
x2_nlpac = interp1(t,x(:,5),ti);
x2_nlpac2 = interp1(t2,x2(:,5),ti);
x2_nlplc = interp1(t3,x3,ti);

%% Adaptive Controller Perturbed Non-Linear System Saw-Tooth
tmax = 300;
traj = @tr3;
trajd = @tr3d;
tspan = [0 tmax];
xc0 = [0;0;0*pi/180;0];

xm0 = xc0;
kx0 = -K';
kr0 = 1;
x0 = [xm0;xc0;kx0;kr0];
[t, x] = ode45( @Tracking_NonLinearmodel_AC_Perturbed , tspan, x0);
kx0 = mean(x(end-50:end,9:12))';
kr0 = mean(x(end-50:end,13));
x0 = [xm0;xc0;kx0;kr0];
[t2, x2] = ode45( @Tracking_NonLinearmodel_AC_Perturbed , tspan, x0);
[t3, x3] = ode45( @Tracking_NonLinearmodel_LC_Perturbed , tspan, xc0);

xtrack = zeros(1,length(t));
xtrackd = zeros(1,length(t));
for i=1:length(t)
    xtrack(i) = traj(t(i));
    xtrackd(i) = trajd(t(i));
end
f11 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(t,x(:,1));
        plot(t,x(:,5));
        plot(t,xtrack,'--k');
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('position (m)')
    legend('ref','adaptive (NLSP)','tracked')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(t,x(:,2));
        plot(t,x(:,6));
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    legend('ref','adaptive (NLSP)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(t,180*x(:,3)/pi);
        plot(t,180*x(:,7)/pi);
    hold off
    grid on;
    legend('ref','adaptive (NLSP)')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(t,180*x(:,4)/pi);
        plot(t,180*x(:,8)/pi);
    hold off
    grid on;
    legend('ref','adaptive (NLSP)')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('System tracking of a sinus saw-tooth');
saveas(f11,'Results/track_AC_NLSP_saw_tooth.png');

f12 = figure('Position',[0 0 800 400]);
    subplot(2,3,1);
    plot(t,x(:,9))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_1')
    title('Kx_1');
    xlim([0 tmax]);
    subplot(2,3,2);
    plot(t,x(:,10))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_2')
    title('Kx_2');
    xlim([0 tmax]);
    subplot(2,3,4);
    plot(t,x(:,11))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_3')
    title('Kx_3');
    xlim([0 tmax]);
    subplot(2,3,5);
    plot(t,x(:,12))
    grid on;
    xlabel('time (s)')
    ylabel('Kx_4')
    title('Kx_4');
    xlim([0 tmax]);
    subplot(2,3,[3 6]);
    plot(t,x(:,13))
    grid on;
    xlabel('time (s)')
    ylabel('Kr')
    title('Kr');  
    xlim([0 tmax]);
    sgtitle('system gains when tracking a saw-tooth');
saveas(f12,'Results/track_AC_NLSP_gains_saw_tooth.png');

% that's ugly refactor it if time allows. Does u computation + interpolation 
ti = [1:50000];
ti = ti*50/50000;
xtrack2 = zeros(1,length(t2));
xtrackd2 = zeros(1,length(t2));
for i=1:length(t2)
    xtrack2(i) = traj(t2(i));
    xtrackd2(i) = trajd(t2(i));
end
r = xtrack*K(1) + xtrackd*K(2);
r2 = xtrack2*K(1) + xtrackd2*K(2);

tmp = [];
tmp2 = [];
for i=1:length(t)
    tmp(i) = x(i,9:12)*x(i,5:8)'+x(i,13).*r(i);
end
for i=1:length(t2)
    tmp2(i) = x2(i,9:12)*x2(i,5:8)'+x2(i,13).*r(i);
end
u3_ref = interp1(t,-K*x(:,1:4)',ti);
u3_ac = interp1(t,tmp,ti);
u3_ac2 = interp1(t2,tmp2,ti);
u3_lc = interp1(t3,-K*x3(:,1:4)',ti);

x3_track_stab = interp1(t,xtrack,ti);
x3_ref = interp1(t,x(:,1),ti);
x3_nlpac = interp1(t,x(:,5),ti);
x3_nlpac2 = interp1(t2,x2(:,5),ti);
x3_nlplc = interp1(t3,x3,ti);

%% Write to file
fileID = fopen('Results/AC_track.txt','w');
fprintf(fileID, '----------------------------------\n');
fprintf(fileID, '            Tracking sin          \n');
fprintf(fileID, ' \n');
fprintf(fileID, 'Command:\n');
fprintf(fileID, 'ref vs LQR Perturbed (MSE): %f\n',mse(u1_ref,u1_lc));
fprintf(fileID, 'ref vs AC1 Perturbed (MSE): %f\n',mse(u1_ref,u1_ac));
fprintf(fileID, 'ref vs AC2 Perturbed (MSE): %f\n',mse(u1_ref,u1_ac2));
fprintf(fileID, '\n');
fprintf(fileID, 'Error:\n');
fprintf(fileID, 'ref vs LQR Perturbed (MSE): %f\n',mse(x1_ref,x1_nlplc ));
fprintf(fileID, 'ref vs AC1 Perturbed (MSE): %f\n',mse(x1_ref,x1_nlpac));
fprintf(fileID, 'ref vs AC2 Perturbed (MSE): %f\n',mse(x1_ref,x1_nlpac2));
fprintf(fileID, '\n');
fprintf(fileID, '----------------------------------\n');
fprintf(fileID, '          Tracking square         \n');
fprintf(fileID, ' \n');
fprintf(fileID, 'Command:\n');
fprintf(fileID, 'ref vs LQR Perturbed (MSE): %f\n',mse(u2_ref,u2_lc));
fprintf(fileID, 'ref vs AC1 Perturbed (MSE): %f\n',mse(u2_ref,u2_ac));
fprintf(fileID, 'ref vs AC2 Perturbed (MSE): %f\n',mse(u2_ref,u2_ac2));
fprintf(fileID, '\n');
fprintf(fileID, 'Error:\n');
fprintf(fileID, 'ref vs LQR Perturbed (MSE): %f\n',mse(x2_ref,x2_nlplc ));
fprintf(fileID, 'ref vs AC1 Perturbed (MSE): %f\n',mse(x2_ref,x2_nlpac));
fprintf(fileID, 'ref vs AC2 Perturbed (MSE): %f\n',mse(x2_ref,x2_nlpac2));
fprintf(fileID, ' \n');
fprintf(fileID, '----------------------------------\n');
fprintf(fileID, '         Tracking saw-tooth       \n');
fprintf(fileID, ' \n');
fprintf(fileID, 'Command:\n');
fprintf(fileID, 'ref vs LQR Perturbed (MSE): %f\n',mse(u3_ref,u3_lc));
fprintf(fileID, 'ref vs AC1 Perturbed (MSE): %f\n',mse(u3_ref,u3_ac));
fprintf(fileID, 'ref vs AC2 Perturbed (MSE): %f\n',mse(u3_ref,u3_ac2));
fprintf(fileID, '\n');
fprintf(fileID, 'Error:\n');
fprintf(fileID, 'ref vs LQR Perturbed (MSE): %f\n',mse(x3_ref,x3_nlplc ));
fprintf(fileID, 'ref vs AC1 Perturbed (MSE): %f\n',mse(x3_ref,x3_nlpac));
fprintf(fileID, 'ref vs AC2 Perturbed (MSE): %f\n',mse(x3_ref,x3_nlpac2));
fprintf(fileID, ' \n');
fprintf(fileID, '----------------------------------\n');
fclose(fileID);
type Results/AC_track.txt


%% Functions
% tracking of a sinus
% signal to track as tr, derivative of signal as trd
function y = tr1(x)
    y = 0.5*sin(x/4);
end
function y = tr1d(x)
    y = 0.5*1/4*cos(x/4);
end

% tracking of a step
% signal to track as tr, derivative of signal as trd
function y = tr2(x)
    if mod(x,16) > 8
        y = 0.5;
    else
        y = -0.5;
    end
end
function y = tr2d(x)
    y = 0;
end

% tracking of a saw-tooth
% signal to track as tr, derivative of signal as trd
function y = tr3(x)
    if mod(x,16) < 8
        y = mod(x,8)*0.25;
    else
        y = (8 - mod(x,8))*0.25;
    end
end
function y = tr3d(x)
    if mod(x,16) < 8
        y = 0.25;
    else
        y = -0.25;
    end
end