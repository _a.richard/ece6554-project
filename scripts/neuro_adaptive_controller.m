%% Neuro Adaptive Controller
clc;
close all;
clear all;

%% Parameters Normal
global mc mp Jp l delta_theta delta_x g M L b Dx Dtheta R Q A B K Am Bm;

mc=0.5;
mp=0.1;
Jp=0.006;
l=0.3;
delta_theta=0.05;
delta_x=0.01;
g=9.8;

M=(mc+mp)/(mp*l);
L=Jp/(mp*l)+l;
b=1/(mp*l);
Dx=delta_x/(mp*l);
Dtheta=delta_theta/(mp*l);

%% Linear model
A=[0,1,0,0;
    0, -L*Dx/(M*L-1), -g/(M*L-1), Dtheta/(M*L-1);
    0, 0,0,1;
    0, Dx/(M*L-1), M*g/(M*L-1), -M*Dtheta/(M*L-1)];

B=[0;
    b*L/(M*L-1);
    0;
    -b/(M*L-1)];

% Use LQR to find the gains.
global P
Q = eye(4);
R = 10;
[K,X,E]=lqr(A,B,Q,R);
P = X;

Am = A - B*K;
Bm = [0;1;0;1];

%% Parameters Perturbed (+20%)
global mc2 mp2 Jp2 l2 delta_theta2 delta_x2 g2 M2 L2 b2 Dx2 Dtheta2;
global A2 B2;

mc2=mc*1.2;
mp2=mp*1.2;
Jp2=Jp*1.2;
l2=l*1.2;
delta_theta2=delta_theta*1.2;
delta_x2=delta_x*1.2;
g2=9.8;

M2=(mc2+mp2)/(mp2*l2);
L2=Jp2/(mp2*l2)+l2;
b2=1/(mp2*l2);
Dx2=delta_x2/(mp2*l2);
Dtheta2=delta_theta2/(mp2*l2);

%% Linear model perturbed
A2=[0,1,0,0;
    0, -L2*Dx2/(M2*L2-1), -g2/(M2*L2-1), Dtheta2/(M2*L2-1);
    0, 0,0,1;
    0, Dx2/(M2*L2-1), M2*g2/(M2*L2-1), -M2*Dtheta2/(M2*L2-1)];

B2=[0;
    b2*L2/(M2*L2-1);
    0;
    -b2/(M2*L2-1)];
%% Neuro-Adaptive controller
global centers nb_neurons sigma gamma alpha_dim
nb_input = 3;
nb_neurons = 13;
bound_x1 = 2;
bound_x2 = 2;
bound_x3 = 2;
sigma = (2*bound_x1)/nb_neurons;
gamma = 10000;

[x, y, z] = meshgrid(-bound_x1:2*bound_x1/(nb_neurons-1):bound_x1,-bound_x2:2*bound_x2/(nb_neurons-1):bound_x2,-bound_x3:2*bound_x3/(nb_neurons-1):bound_x3);
centers =[x(:) y(:) z(:)]';
alpha_dim = size(centers,2);
alpha0 = rand(alpha_dim,1);
%% Stabilizing Neuro Adaptive Controller on Non-Linear System usually working ranges
tmax = 8;
tspan = [0 tmax];

x = {};
t = {};
j = 1;
for i=0:5:60
    xm0 = [0;0;i*pi/180;0];
    xc0 = xm0;
    X0 = [xm0;xc0;alpha0];
    [t{j}, x{j}] = ode45(@NonLinearmodel_NeuroAC , tspan, X0);
    alpha0 = mean(x{j}(end-20:end,9:end))';
    j = j+1;
end
save('Weights/stabilization_weights.mat','alpha0')

% Commented reference plot: only adds confusion. MAKE APPROPRIATE EDIT
% BEFORE SUBMISSION.

f0 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
    for i=1:length(x)
%         plot(t{i},x{i}(:,1));
        plot(t{i},x{i}(:,5));
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('position (m)')
    title('linear position');

    subplot(2,2,2);
    hold on
    for i=1:length(x)
%         plot(t{i},x{i}(:,2));
        plot(t{i},x{i}(:,6));
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
    for i=1:length(x)
%         plot(t{i},180*x{i}(:,3)/pi);
        plot(t{i},180*x{i}(:,7)/pi);
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
    for i=1:length(x)
%         plot(t{i},180*x{i}(:,4)/pi);
        plot(t{i},180*x{i}(:,8)/pi);
    end
    hold off
    grid on;
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('Neuro AC on NLS stabilization with \theta_0 = 0� to 60� and xm = 0m');
saveas(f0,'Results/stab_NAC_NLS_for_different_safe_theta0.png');
%% Stabilizing NAC on NLS unsafe ranges, stabilization weights.
tmax = 8;
tspan = [0, tmax];
load('Weights/stabilization_weights.mat','alpha0')
xm0 = [0;0;62*pi/180;0];
xc0 = xm0;
X0 = [xm0;xc0;alpha0];
[t, x] = ode45(@NonLinearmodel_NeuroAC , tspan, X0);
alpha0 = mean(x(end-20:end,9:end))';
save('Weights/stabilization_weights_62degrees.mat','alpha0')
f1 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
    plot(t,x(:,1));
    plot(t,x(:,5));
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('position (m)')
    title('linear position');

    subplot(2,2,2);
    hold on
    plot(t,x(:,2));
    plot(t,x(:,6));
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
    plot(t,180*x(:,3)/pi);
    plot(t,180*x(:,7)/pi);
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
    plot(t,180*x(:,4)/pi);
    plot(t,180*x(:,8)/pi);
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('Neuro AC on NLS stabilization with \theta_0 = 62� xm = 0m');
saveas(f1,'Results/stab_NAC_NLS_for_unsafe_theta0_62d_0m.png');

xm0 = [0;0;64*pi/180;0];
xc0 = xm0;
X0 = [xm0;xc0;alpha0];
[t, x] = ode45(@NonLinearmodel_NeuroAC , tspan, X0);
alpha0 = mean(x(end-20:end,9:end))';
save('Weights/stabilization_weights_64degrees.mat','alpha0')

f2 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
    plot(t,x(:,1));
    plot(t,x(:,5));
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('position (m)')
    title('linear position');

    subplot(2,2,2);
    hold on
    plot(t,x(:,2));
    plot(t,x(:,6));
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
    plot(t,180*x(:,3)/pi);
    plot(t,180*x(:,7)/pi);
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
    plot(t,180*x(:,4)/pi);
    plot(t,180*x(:,8)/pi);
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('Neuro AC on NLS stabilization with \theta_0 = 64� xm = 0m');
saveas(f2,'Results/stab_NAC_NLS_for_unsafe_theta0_64d_0m.png');

xm0 = [0;0;66*pi/180;0];
xc0 = xm0;
X0 = [xm0;xc0;alpha0];
[t, x] = ode45(@NonLinearmodel_NeuroAC , tspan, X0);
alpha0 = mean(x(end-20:end,9:end))';
save('Weights/stabilization_weights_66degrees.mat','alpha0')

f3 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
    plot(t,x(:,1));
    plot(t,x(:,5));
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('position (m)')
    title('linear position');

    subplot(2,2,2);
    hold on
    plot(t,x(:,2));
    plot(t,x(:,6));
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
    plot(t,180*x(:,3)/pi);
    plot(t,180*x(:,7)/pi);
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
    plot(t,180*x(:,4)/pi);
    plot(t,180*x(:,8)/pi);
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('Neuro AC on NLS stabilization with \theta_0 = 66� xm = 0m');
saveas(f3,'Results/stab_NAC_NLS_for_unsafe_theta0_66d_0m.png');

xm0 = [0;0;68*pi/180;0];
xc0 = xm0;
X0 = [xm0;xc0;alpha0];
[t, x] = ode45(@NonLinearmodel_NeuroAC , tspan, X0);
alpha0 = mean(x(end-20:end,9:end))';
save('Weights/stabilization_weights_68degrees.mat','alpha0')

f4 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
    plot(t,x(:,1));
    plot(t,x(:,5));
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('position (m)')
    title('linear position');

    subplot(2,2,2);
    hold on
    plot(t,x(:,2));
    plot(t,x(:,6));
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
    plot(t,180*x(:,3)/pi);
    plot(t,180*x(:,7)/pi);
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
    plot(t,180*x(:,4)/pi);
    plot(t,180*x(:,8)/pi);
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('Neuro AC on NLS stabilization with \theta_0 = 68� xm = 0m');
saveas(f4,'Results/stab_NAC_NLS_for_unsafe_theta0_68d_0m.png');
f4 = figure('Position',[0 0 800 400]);
plot(t,x(:,9:end));
title('\alpha parameters')
xlabel('time (s)')
ylabel('\alpha')
saveas(f4,'Results/stab_NAC_NLS_for_unsafe_theta0_68d_0m_alpha.png');


xm0 = [0;0;70*pi/180;0];
xc0 = xm0;
X0 = [xm0;xc0;alpha0];
[t, x] = ode45(@NonLinearmodel_NeuroAC , tspan, X0);
alpha0 = mean(x(end-20:end,9:end))';
save('Weights/stabilization_weights_70degrees.mat','alpha0')

f4 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
    plot(t,x(:,1));
    plot(t,x(:,5));
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('position (m)')
    title('linear position');

    subplot(2,2,2);
    hold on
    plot(t,x(:,2));
    plot(t,x(:,6));
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
    plot(t,180*x(:,3)/pi);
    plot(t,180*x(:,7)/pi);
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
    plot(t,180*x(:,4)/pi);
    plot(t,180*x(:,8)/pi);
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('Neuro AC on NLS stabilization with \theta_0 = 70� xm = 0m');
saveas(f4,'Results/stab_NAC_NLS_for_unsafe_theta0_70d_0m.png');

xm0 = [0;0;72*pi/180;0];
xc0 = xm0;
X0 = [xm0;xc0;alpha0];
[t, x] = ode45(@NonLinearmodel_NeuroAC , tspan, X0);
alpha0 = mean(x(end-20:end,9:end))';
save('Weights/stabilization_weights_72degrees.mat','alpha0')

f4 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
    plot(t,x(:,1));
    plot(t,x(:,5));
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('position (m)')
    title('linear position');

    subplot(2,2,2);
    hold on
    plot(t,x(:,2));
    plot(t,x(:,6));
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
    plot(t,180*x(:,3)/pi);
    plot(t,180*x(:,7)/pi);
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
    plot(t,180*x(:,4)/pi);
    plot(t,180*x(:,8)/pi);
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('Neuro AC on NLS stabilization with \theta_0 = 72� xm = 0m');
saveas(f4,'Results/stab_NAC_NLS_for_unsafe_theta0_72d_0m.png');
%% Not working one
load('Weights/stabilization_weights_72degrees.mat','alpha0')
tmax = 5;
tspan = [0 tmax];
xm0 = [0;0;74*pi/180;0];
xc0 = xm0;
X0 = [xm0;xc0;alpha0];
[t, x] = ode45(@NonLinearmodel_NeuroAC , tspan, X0);

f5 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
    plot(t,x(:,1));
    plot(t,x(:,5));
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('position (m)')
    title('linear position');

    subplot(2,2,2);
    hold on
    plot(t,x(:,2));
    plot(t,x(:,6));
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
    plot(t,180*x(:,3)/pi);
    plot(t,180*x(:,7)/pi);
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('angular position (�)')
    title('angular position');

    subplot(2,2,4);
    hold on
    plot(t,180*x(:,4)/pi);
    plot(t,180*x(:,8)/pi);
    hold off
    grid on;
    legend('ref','NAC')
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    title('angular velocity');
    sgtitle('Neuro AC on NLS stabilization with \theta_0 = 74� xm = 0m');
saveas(f5,'Results/stab_NAC_NLS_for_unsafe_theta0_74d_0m.png');

%% Tracking
global traj trajd;

%% Tracking Neuro Adaptive Controller on Non-Linear System: Sinus
traj = @tr1;
trajd = @tr1d;

alpha0 = rand(alpha_dim,1);
tmax = 30;
tspan = [0 tmax];
xm0 = [0;0;72*pi/180;0];
xc0 = xm0;
X0 = [xm0;xc0;alpha0];
[t, x] = ode45( @Tracking_NonLinearmodel_NeuroAC , tspan, X0);
alpha0 = mean(x(end-20:end,9:end))';

save('Weights/tracking_weights_sinus.mat','alpha0')
xtrack = traj(t);

f6 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(t,x(:,1));
        plot(t,x(:,5));
        plot(t,xtrack,'--k');
    hold off
    grid on
    xlabel('time (s)')
    ylabel('position (m)')
    legend('ref','NAC','tracked')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(t,x(:,2));
        plot(t,x(:,6));
    hold off
    grid on
    xlabel('time (s)')
    legend('ref','NAC')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(t,180*x(:,3)/pi);
        plot(t,180*x(:,7)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular position (�)')
    legend('ref','NAC')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(t,180*x(:,4)/pi);
        plot(t,180*x(:,8)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    legend('ref','NAC')
    title('angular velocity');
    sgtitle('Tracking of a reference signal 1/2*sin(1/4t)');
saveas(f6,'Results/track_NAC_NLS_sin_72.png');

%% Tracking Neuro Adaptive Controller on Non-Linear System: Square
traj = @tr2;
trajd = @tr2d;

tmax = 30;
tspan = [0 tmax];
xm0 = [0;0;72*pi/180;0];
xc0 = xm0;
X0 = [xm0;xc0;alpha0];
[t, x] = ode45( @Tracking_NonLinearmodel_NeuroAC , tspan, X0);

alpha0 = mean(x(end-20:end,9:end))';
xm0 = [0;0;73*pi/180;0];
xc0 = xm0;
X0 = [xm0;xc0;alpha0];
[t, x] = ode45( @Tracking_NonLinearmodel_NeuroAC , tspan, X0);
alpha0 = mean(x(end-20:end,9:end))';

xtrack = zeros(1,length(t));
for i=1:length(t)
    xtrack(i) = traj(t(i));
end

f7 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(t,x(:,1));
        plot(t,x(:,5));
        plot(t,xtrack,'--k');
    hold off
    grid on
    xlabel('time (s)')
    ylabel('position (m)')
    legend('ref','NAC','tracked')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(t,x(:,2));
        plot(t,x(:,6));
    hold off
    grid on
    xlabel('time (s)')
    legend('ref','NAC')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(t,180*x(:,3)/pi);
        plot(t,180*x(:,7)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular position (�)')
    legend('ref','NAC')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(t,180*x(:,4)/pi);
        plot(t,180*x(:,8)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    legend('ref','NAC')
    title('angular velocity');
    sgtitle('Tracking of a square reference signal');
saveas(f7,'Results/track_NAC_NLS_square_73.png');

%% Tracking Neuro Adaptive Controller on Non-Linear System: Saw Tooth
traj = @tr3;
trajd = @tr3d;

tmax = 30;
tspan = [0 tmax];
xm0 = [0;0;72*pi/180;0];
xc0 = xm0;
X0 = [xm0;xc0;alpha0];
[t, x] = ode45( @Tracking_NonLinearmodel_NeuroAC , tspan, X0);

alpha0 = mean(x(end-20:end,9:end))';
xm0 = [0;0;73*pi/180;0];
xc0 = xm0;
X0 = [xm0;xc0;alpha0];
[t, x] = ode45( @Tracking_NonLinearmodel_NeuroAC , tspan, X0);
alpha0 = mean(x(end-20:end,9:end))';

xtrack = zeros(1,length(t));
for i=1:length(t)
    xtrack(i) = traj(t(i));
end

f8 = figure('Position',[0 0 800 400]);
    subplot(2,2,1);
    hold on
        plot(t,x(:,1));
        plot(t,x(:,5));
        plot(t,xtrack,'--k');
    hold off
    grid on
    xlabel('time (s)')
    ylabel('position (m)')
    legend('ref','NAC','tracked')
    title('linear position');

    subplot(2,2,2);
    hold on
        plot(t,x(:,2));
        plot(t,x(:,6));
    hold off
    grid on
    xlabel('time (s)')
    legend('ref','NAC')
    ylabel('velocity (m/s)')
    title('linear velocity');

    subplot(2,2,3);
    hold on
        plot(t,180*x(:,3)/pi);
        plot(t,180*x(:,7)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular position (�)')
    legend('ref','NAC')
    title('angular position');

    subplot(2,2,4);
    hold on
        plot(t,180*x(:,4)/pi);
        plot(t,180*x(:,8)/pi);
    hold off
    grid on
    xlabel('time (s)')
    ylabel('angular velocity (�/s)')
    legend('ref','NAC')
    title('angular velocity');
    sgtitle('Tracking of a saw-tooth reference signal');
saveas(f8,'Results/track_NAC_NLS_saw_tooth_73.png');