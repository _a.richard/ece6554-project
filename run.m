%% RUN STEP BY STEP for the different steps of the project

%% RUN THIS STEP BEFORE RUNNING ANYTHING ELSE OR IT WILL OUTPUT ERRORS
clear all;
close all;
clc;
mkdir('Results')
mkdir('Weights')
% Add the path to the ode functions and the scripts that uses them.
addpath('ode_functions');
addpath('functions');
addpath('scripts');

%% Nomenclature to understand the name of the figures.

% LC == LQR referes to the linear controller.
% AC referes to the adaptive controler.
% NLC referes to non-linearity compensation

% LS referes to the linear system
% LSP referes to the linear system with perturbation
% NLS referes to the non-linear-system
% NLSP referes to the non-linear system with perturbation

% stab referes to stability resutls
% track referes to tracking results

%% Linearized/Non-Linear System with Linear Controller
linear_controller;

% Conclusion: After analysis the linear system works for all values of 
% Theta_0. However, the non-linear system has a bound at 60 degree: above
% this value the system will diverge.

%% Linearized/Non-Linear System with Linear Tracking Controller
linear_tracking_controller

% Conclusion: Using a suitable command law the system can follow saw-tooth,
% sinusoids and square. For the sinusoid and the saw-tooth, it tends to
% strongly overshoot, yet it works well, signals are almost in phase, 
% and the error is reasonable. Overall observation the linear controller
% does not manage to track the signal perfectly due to the fact that this
% system is under-actuated and hence the controler is a trade off between
% keeping the pole up-right and tracking the x position.

%% System Perturbation Linear Controller
perturbation_linear_controller

% Conclusion: The perturbation has a small impact on the reponse of the
% system for both the stabilization and the tracking. However, the
% perturbation (on the values of the system) really increases the 
% command cost. Meaning in a real system with current constraints on the
% motor we may have degraded performances.

%% System Stabilization with Adaptive Controller
adaptive_controller_stabilization

% Conclusion: The adaptive controller really doesn't like the non-linearity
% on u. Trying to compensate for it like we'd do in feedback linearization
% only makes the matter worse. Using a non-linearity compensation on the
% input is therefor out of the question for the rest of the project since
% it strongly degrades the performance. Plus there is a issue when theta
% goes to 90 degrees.

%% System Perturbation Tracking Adaptive Controller
adaptive_controller_tracking

% Conclusion: Adaptive control achieves much better tracking of the
% reference signal. Running the adaptive controller 2 times gives way
% better results on the second run depending on the tracked signal. Also we
% can notice that the control input is way more expensive in the case of
% the adaptive controller.

%% Basic Neuro-Adaptive Controller
neuro_adaptive_controller_test

% Conclusion: Works perfectly if I may say so :).

%% Neuro-Adaptive Controller
neuro_adaptive_controller

% Conclusion: WIP