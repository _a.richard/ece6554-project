function y = tr3d(x)
    if mod(x,16) < 8
        y = 0.25;
    else
        y = -0.25;
    end
end

