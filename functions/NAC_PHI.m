function [phi] = NAC_PHI(centers,sigma,x)
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here
norm = sum(bsxfun(@minus,x,centers).^2);
phi = exp(-(norm.^2)./(2*sigma^2));
end

