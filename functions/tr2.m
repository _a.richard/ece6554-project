function y = tr2(x)
    if mod(x,16) > 8
        y = 0.5;
    else
        y = -0.5;
    end
end