function [phi] = NAC_PHI2(centers,sigma,x)
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here
norm = bsxfun(@minus,x,centers).^2;
phi = exp(-(norm.^2)./(sigma^2));
end

