function y = tr3(x)
    if mod(x,16) < 8
        y = mod(x,8)*0.25;
    else
        y = (8 - mod(x,8))*0.25;
    end
end